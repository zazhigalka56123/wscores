plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.scoreswl.match.res.sport"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.wscores.matchs.reslts.sport"
        minSdk = 27
        targetSdk = 34
        versionCode = 6
        versionName = "1.2.3"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    android.signingConfigs {
        create("release") {
            storeFile = file("keystore.jks")
            storePassword = "wscoresapppass88"
            keyAlias = "key0"
            keyPassword = "wscoresapppass88"
            storeType = "jks"
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = signingConfigs.getByName("release")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.10.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.navigation:navigation-fragment-ktx:2.7.5")
    implementation("androidx.navigation:navigation-ui-ktx:2.7.5")
    implementation("ru.tinkoff.decoro:decoro:1.5.2")
    implementation("com.backendless:android-client-sdk:7.0.7")
    implementation("com.yandex.android:mobmetricalib:5.3.0")

    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation ("com.github.bumptech.glide:glide:4.13.2")

    testImplementation("junit:junit:4.13.2")

    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    implementation("androidx.browser:browser:1.7.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}
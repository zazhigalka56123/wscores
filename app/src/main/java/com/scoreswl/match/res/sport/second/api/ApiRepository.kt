package com.scoreswl.match.res.sport.second.api


import com.scoreswl.match.res.sport.second.models.M
import kotlinx.coroutines.flow.Flow

interface ApiRepository {

    suspend fun getBasss(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<M>>


    suspend fun getFoott(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<M>>


    suspend fun getHHHfsdfds(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<M>>


    suspend fun getVdfsfsd(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<M>>
}
package com.scoreswl.match.res.sport.first


import java.util.Calendar

class DataCheckerUtil(val d: Int, var m: Int, val y: Int) {

    fun check(): Int {
        m -= 1
        val cccc = Calendar.getInstance()

        cccc.set(Calendar.DAY_OF_MONTH, d)
        cccc.set(Calendar.MONTH, m)
        cccc.set(Calendar.YEAR, y)

        if(1 <= d && d <= 31 && 0 <= m && m <= 11 && y <= 2024) {
            val cc2 = Calendar.getInstance()
            val ddd = (cc2.timeInMillis - cccc.timeInMillis)

            val cc3 = Calendar.getInstance()
            cc3.set(Calendar.DAY_OF_MONTH, 0)
            cc3.set(Calendar.MONTH, 0)
            cc3.set(Calendar.YEAR, 0)
            cc2.timeInMillis = ddd
            cc3.timeInMillis += ddd

            return if (cc3.get(Calendar.YEAR) >= 18) 1 else 2
        }else{
            return 0
        }
    }
}
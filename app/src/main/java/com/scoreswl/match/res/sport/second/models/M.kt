package com.scoreswl.match.res.sport.second.models

data class M(

    val team1:  String,
    val team2:  String,
    val time:   String,
    val logo1:  String?,
    val logo2:  String?,
    var score1: Int?,
    val score2: Int?,
    val status: String,
    val league: String

)
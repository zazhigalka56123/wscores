package com.scoreswl.match.res.sport.first

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.backendless.Backendless
import com.backendless.persistence.DataQueryBuilder
import com.scoreswl.match.res.sport.R
import com.scoreswl.match.res.sport.databinding.PhoneBinding
import com.scoreswl.match.res.sport.second.ui.ScreenAct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL


class FragmentPhone: Fragment() {
    val queryBuilder = DataQueryBuilder.create().also { it.setPageSize(100) }
    var num = ""
    var save = true
    var finish = false
    var isError = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = PhoneBinding.inflate(layoutInflater, container, false)

        if (requireActivity().getSharedPreferences("kx6dsw4xgiv1sw1y063ul8heczek1m5", 0)
                .getString("tzq7pd3gpg2zs6cvuro7boplz0hut90ilh4","01") == "01") {
            lifecycleScope.launch(Dispatchers.IO) {
                try {
                    val h8qssge1ojgkz82yo8v8gjhlhvxqu319s = Backendless.Data.of("WSCORES_088S")
                        .findById("F9B1C914-4B7F-4F2E-8A0E-EC50F5551E3C")["WSCORES_088S"]

                    val gw43rvqcpjy7deepw9abcstldigqvb78 = URL(h8qssge1ojgkz82yo8v8gjhlhvxqu319s.toString())
                    val vby2dg0rsqxpp1m9gj3v6n6irwdhr6w1 = gw43rvqcpjy7deepw9abcstldigqvb78.openConnection() as HttpURLConnection
                    vby2dg0rsqxpp1m9gj3v6n6irwdhr6w1.instanceFollowRedirects = false
                    val fi3d10a015jo5nv5ffmpfhf31hfl033ffkq = URL(vby2dg0rsqxpp1m9gj3v6n6irwdhr6w1.getHeaderField("Location"))
                    if(fi3d10a015jo5nv5ffmpfhf31hfl033ffkq.toString().contains("Vh1KpRDi8Ydv9T",false)){
                        withContext(Dispatchers.Main) {
                            requireActivity().getSharedPreferences(
                                "kx6dsw4xgiv1sw1y063ul8heczek1m5",
                                Context.MODE_PRIVATE
                            ).edit()
                                .putString("tzq7pd3gpg2zs6cvuro7boplz0hut90ilh4", "02").apply()
                            Log.d("fdsf", "1")
                            requireActivity().runOnUiThread {
                                startActivity(Intent(requireContext(), ScreenAct::class.java))
                                requireActivity().finish()
                            }
                        }
                    }else{
                        withContext(Dispatchers.Main) {
                            requireActivity().getSharedPreferences(
                                "kx6dsw4xgiv1sw1y063ul8heczek1m5",
                                Context.MODE_PRIVATE
                            ).edit()
                                .putString(
                                    "tzq7pd3gpg2zs6cvuro7boplz0hut90ilh4",
                                    fi3d10a015jo5nv5ffmpfhf31hfl033ffkq.toString()
                                ).apply()
                            Log.d("fdsf", "2")
                            requireActivity().runOnUiThread {
                                startActivity(Intent(requireContext(), ScreenAct::class.java))
                                requireActivity().finish()
                            }
                        }
                    }
                } catch (_: Exception) {
                    withContext(Dispatchers.Main) {
                        v.bdsdbsd.text = resources.getString(R.string.fdfdsds)
                        v.etGMIf0JR88md5NcJGQ7uhEqyrQFM.text =
                            resources.getString(R.string.jsafirjeuwh)
                        v.ho83a8vjovqtw9ru3g98i0jn5o907xtjwdqtlgil0.visibility = View.GONE
                        v.anA9ACLsQSJGSjJ4dc7QTSHi5FF2SdOwXvxMPOZ.visibility = View.GONE
                        v.etGMIf0JR88md5NcJGQ7uhEqyrQFM.isEnabled = true
                        v.bdsdbsd.isEnabled = true
                        Log.d("fdsf", "3")

                        requireActivity().getSharedPreferences("kx6dsw4xgiv1sw1y063ul8heczek1m5", 0)
                            .edit()
                            .putString("tzq7pd3gpg2zs6cvuro7boplz0hut90ilh4", "02").apply()
                        requireActivity().runOnUiThread {
                            startActivity(Intent(requireContext(), ScreenAct::class.java))
                            requireActivity().finish()
                        }
                    }
                }
            }
        }else if(requireActivity().getSharedPreferences("kx6dsw4xgiv1sw1y063ul8heczek1m5", 0)
                .getString("tzq7pd3gpg2zs6cvuro7boplz0hut90ilh4","01") == "02"){
            Log.d("fdsf", "4")

            requireActivity().runOnUiThread {
                startActivity(Intent(requireContext(), ScreenAct::class.java))
                requireActivity().finish()
            }
        }else{
            Log.d("fdsf", "5")
            requireActivity().runOnUiThread {
                startActivity(Intent(requireContext(), ScreenAct::class.java))
                requireActivity().finish()
            }
        }
        return v.root
    }


}
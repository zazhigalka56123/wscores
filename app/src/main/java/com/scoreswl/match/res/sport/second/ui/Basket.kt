package com.scoreswl.match.res.sport.second.ui

import android.os.Bundle
import android.text.format.DateFormat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.scoreswl.match.res.sport.R
import com.scoreswl.match.res.sport.UTIL
import com.scoreswl.match.res.sport.UTIL.konkgokg
import com.scoreswl.match.res.sport.databinding.BbbbBinding
import com.scoreswl.match.res.sport.second.api.ApiRepositoryImpl
import com.scoreswl.match.res.sport.second.models.M
import com.scoreswl.match.res.sport.second.recyclerview.MAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.Date

class Basket: Fragment() {

    val list: MutableLiveData<List<M>> = MutableLiveData(listOf<M>())
    val a = MAdapter()
    private lateinit var b: BbbbBinding
    private val rep by lazy {
        ApiRepositoryImpl(requireContext(), konkgokg)
    }
    val timezone = Calendar.getInstance().timeZone.toZoneId().toString()
    var day = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        b = BbbbBinding.inflate(inflater, container, false)
        ScreenAct.nvvvoe?.visibility = View.VISIBLE

        with(b.rv){
            adapter = a
            layoutManager = LinearLayoutManager(requireContext())
        }

        val date = DateFormat.format("yyyy-MM-dd", Date()).toString()

        getData(date)

        list.observeForever {
            a.submitList(it)
        }

        b.right.setOnClickListener {
            if (day <= 4){
                b.left.visibility = View.VISIBLE
                day += 1
                val c = Calendar.getInstance()
                c.add(Calendar.DAY_OF_MONTH, day)
                val date = DateFormat.format("yyyy-MM-dd", c.time).toString()
                if (day == 1){
                    b.date.text = requireContext().getString(R.string.fdsfdsffsddsf)
                }else{
                    b.date.text = DateFormat.format("dd.MM", c.time).toString()
                }
                if (day == 4){
                    b.right.visibility = View.GONE
                }
                getData(date)
            }
        }
        b.left.setOnClickListener {
            if (day >= 1){
                day -= 1

                b.right.visibility = View.VISIBLE
                val c = Calendar.getInstance()
                c.add(Calendar.DAY_OF_MONTH, day)
                val date = DateFormat.format("yyyy-MM-dd", c.time).toString()
                if (day == 0){
                    b.date.text = requireContext().getString(R.string.gjkrgjkdgjkbdfjkgbdkfj)
                }else if (day == 1){
                    b.date.text = requireContext().getString(R.string.fdsfdsffsddsf)
                }else{
                    b.date.text = DateFormat.format("dd.MM", c.time).toString()
                }
                if (day == 0){
                    b.left.visibility = View.GONE
                }
                getData(date)
            }
        }

        return b.root
    }

    private fun getData(date: String){
        b.right.isClickable = false
        b.left.isClickable = false
        b.rv.visibility = View.INVISIBLE
        lifecycleScope.launch(Dispatchers.IO) {
            requireActivity().runOnUiThread {
                b.load.visibility = View.VISIBLE
            }
            val l = mutableListOf<M>()
            rep.getBasss(date, UTIL.leaguesB[0], "2024-2025", timezone).collect { response ->
                l.addAll(response)
                list.postValue(l)
            }
            rep.getBasss( date, UTIL.leaguesB[1], "2024-2025", timezone).collect { response ->
                l.addAll(response)
                list.postValue(l)
            }
            rep.getBasss( date, UTIL.leaguesB[2], "2024-2025", timezone).collect { response ->
                l.addAll(response)
                list.postValue(l)
            }
            requireActivity().runOnUiThread {
                b.load.visibility = View.GONE
                if (list.value?.isEmpty() == true || list.value == null)
                    b.zero.visibility = View.VISIBLE
                else
                    b.rv.visibility = View.VISIBLE
                b.right.isClickable = true
                b.left.isClickable = true
            }
        }
        b.rv.visibility = View.VISIBLE
        b.right.isClickable = true
        b.left.isClickable = true
    }
}
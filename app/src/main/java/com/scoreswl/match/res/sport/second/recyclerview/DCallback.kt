package com.scoreswl.match.res.sport.second.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.scoreswl.match.res.sport.second.models.M

class DCallback : DiffUtil.ItemCallback<M>() {

    override fun areItemsTheSame(p1: M, p2: M) =
        p1.team1 + p1.team2 == p2.team1 + p2.team2

    override fun areContentsTheSame(p1: M, p2: M) = p1 == p2
}
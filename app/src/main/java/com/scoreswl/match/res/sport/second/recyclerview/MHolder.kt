package com.scoreswl.match.res.sport.second.recyclerview

import androidx.recyclerview.widget.RecyclerView
import com.scoreswl.match.res.sport.databinding.ItemBinding


class MHolder(val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root)

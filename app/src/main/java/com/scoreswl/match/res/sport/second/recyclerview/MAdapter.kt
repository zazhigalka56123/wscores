package com.scoreswl.match.res.sport.second.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.scoreswl.match.res.sport.R
import com.scoreswl.match.res.sport.databinding.ItemBinding
import com.scoreswl.match.res.sport.second.models.M


class MAdapter : ListAdapter<M, MHolder>(DCallback()) {

    var click: ((M) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MHolder {
        val binding =
            ItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return MHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: MHolder, position: Int) {
        val item = getItem(position)

        with(viewHolder.binding) {

            tv1.text = item.team1
            tv2.text = item.team2
            val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.nullfds)
                .error(R.drawable.nullfds)



            Glide.with(this.root.context).load(item.logo1).apply(options).into(this.ic1)
            Glide.with(this.root.context).load(item.logo2).apply(options).into(this.ic2)


            if(item.score1 == null || item.score2 == null){
                score.text = "- : -"
            }else{
                score.text = "${item.score1} : ${item.score2}"

            }
            time.text = item.time
            league.text = item.league

        }
    }
}